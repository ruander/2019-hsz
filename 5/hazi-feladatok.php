<?php
//12.Írjon egy php programot, amely előállít egy XxY cellát tartalmazó táblázatot és mindegyik cellában a Google szót helyezi el a cellák jobb felső sarkába.
$x=3;//sor
$y=5;//oszlop
//egy változóban gyüjtöm a kiírandó elemeket
$table = '<table>';//table nyitás

for($i=1;$i<=$x;$i++){//ciklus a soroknak
    $table .= '<tr>';//sor nyitás
        for($j = 1; $j<=$y;$j++){//ciklus a celláknak
            $table .='<td>google</td>';//cella illesztése a táblázatba
        }
    $table .='</tr>';//sor zárás
}

$table .='</table>';//table zárás
echo $table;
//13.Állítson elő php kóddal egy űrlapot, amely tartalmaz egy szövegmezőt, egy elküldő gombot, egy jelölő négyzetet és egy lenyíló listát, amelyben a hét napjait sorolja fel.
$form = '<form>';
$form .= '<br><input type="text" name="szoveg">';
$form .= '<br><input type="checkbox" name="chkbox" value="1"> jelölő';//jelölő
//tömb a napokkal
$days = [
  'hétfő','kedd','szerda',
];
$form .= '<br><select name="kivalasztas">';
//opciók a days tömb bejárásával
    foreach($days as $day){
    $form .= '<option value="'.$day.'">'.$day.'</option>';
    }
$form .= '</select>';

$form .= '<br><input type="submit" name="submit" value="Gyííííí">';//elküld
$form .='</form>';
echo $form;//űrlap kiírása

//22.Írjon egy programot, amelyben egy 10 elemű tömböt tetszőleges értékekkel tölt fel, majd kiírja, hogy a tömbben hány darab olyan szám van, amely nagyobb, mint 10 és kisebb 20.
//egy 1-100 ig terjedő értékekkel feltöltött tömb
$tomb=[];
$db = 0;
while(count($tomb)<10){
    $number = rand(1,30);
    $tomb[]=$number;
    if($number > 10 and $number < 20) $db++;
}


//később egy rutinosabb megoldás: //a tömb kezelő functionoket nézzétek át: https://www.php.net/manual/en/book.array.php
/*$tomb = range(1,100);
shuffle($tomb);//keverjük össze véletlenszerűen az elemeket
$tomb = array_slice($tomb,0,10);
var_dump($tomb);

//járjuk be és mondjuk meg hány a feltételeknek megfelelő elemet találtunk
$db=0;
foreach($tomb as $number){
    if($number > 10 and $number < 20) $db++;
}*/
echo "<br>ennyi: $db db 10 és 20 közötti érték volt ebben: ".implode(',',$tomb);
echo '<br>';
//19
/*
2
22
222
2222
 */
$text = '2';
for($i=1;$i<=4;$i++){//ciklus a soroknak

    for($j=1;$j<=$i;$j++){//belső ciklus ami mindig a külső ciklusváltozó aktuális értékéig léptet
        echo $text;//text kiírása
    }
    echo '<br>';//a végén uj sor
}
//ugyanaz, ha ismerjük az str_repeat() eljárást
for($i=1;$i<=4;$i++){
    echo '<br>'.str_repeat($text,$i);
}
//17.Írjon egy programot, amelyben egy 20 elemű tetszőleges tömböt hoz létre és kiírja a tömb minden második elemét.
$tomb=['a'=> 13,123,63,653,743,74,65764,3453,6365,332465,635565,354653,3635,563,'ikjhik',53643,];
$kiir=false;
var_dump($tomb);
//bejárjuk a tömböt
foreach($tomb as $v){
    //ha a kiir változó igaz, kiírjuk az aktuális értéket, és átbillentyük a kiir változót ellenkezőjére, ha a kiir hamis, akkor nem csinálunk semmit csak az ellenkezőjére billentyük a kiir változót
    if($kiir){
        echo '<br>'.$v;
        $kiir = false;
    }else{
        $kiir=true;
    }
}
//segéd változóval
$i=1;
foreach($tomb as $v){
    //ha a segédváltozó páros, kiirjuk az aktuális értéket
    if($i%2==0){
        echo '<br>'.$v;
    }
    $i++;
}