<?php
//lotto logika: 5 a 90 ből
//90ből kell véletlenszerűen 1 számot választani
//a kiválasztott szám csak 1szer szerepelhet
//emelkedő számsorrendbe kell rendezni
//-----------------eddig volt a véletlenszerű gépi lottó----
//ugyanilyen folyamat a 'sorsolás' is és így kapunk 2 számsort
//a tároláshoz célszerűen tömböt használunk
//a metszetük megadja a találatok számát
//erőforrások (resources)
echo '<pre>';
$huzasok_szama = 5;
$limit = 90;
$szamok = [];

while(count($szamok)<$huzasok_szama){
    $szamok[] = rand(1,$limit);//új 'sorsolt' szám
    $szamok = array_unique($szamok);//ismétlődések megszüntetése
}
var_dump($szamok);
//rendezés
sort($szamok);//rendezés sort-tal, ami referenciát kap a művelethez azaza 'belenyúl a tömbbe'
var_dump($szamok);


$szamok_eljarassal = lottoGeneralas(45,6);
var_dump($szamok_eljarassal);
//gyártsunk a lottóból újrahasznosítható eljárást

/**
 * Eljárás a számsorsolás szimulálására a lottójátékhoz
 * @param int $huzasok_szama
 * @param int $limit
 * @return array
 */
function lottoGeneralas($huzasok_szama = 5, $limit = 90){
    $szamok = [];

    if($huzasok_szama>$limit){////védelem:végtelen ciklus lenne,ha több szám kéne, mint amiből sorsolni lehet
        trigger_error('Rossz paraméterezés!');
    }else{

        while (count($szamok) < $huzasok_szama) {
            $szamok[] = rand(1, $limit);//új 'sorsolt' szám
            $szamok = array_unique($szamok);//ismétlődések megszüntetése
        }
        sort($szamok);//rendezés
    }
    return $szamok;//visszatérünk a generált tömbbel
}