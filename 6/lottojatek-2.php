<?php
require "functions.php";//functions betöltése
//erőforrások
$limit = 90;
$huzasok_szama = 5;
$szelvenyek = [];//ebbe gyűjtjük a tippsorokat
//töltsük fel tippekkel
while ( count($szelvenyek) < 10) {
    $szelvenyek[] = lottoGeneralas($huzasok_szama, $limit);
}

//generáljunk egy ujabb tippsort, ez lesz a sorsolás
$sorsolas = lottoGeneralas($huzasok_szama,$limit);
echo '<h2>Sorsolt számok:'.implode(',',$sorsolas).'</h2>';//kiírjuk a sorsolás eredményét
//a találatvizsgálat szeelvényenként történik, egyből ki is írjuk a találatokat most
foreach($szelvenyek as $szelveny){
    $talalatok = array_intersect($sorsolas,$szelveny);//aktuális szelvény vizsgálata
    $output = '<h2>tippsor:'.implode(',',$szelveny).'-';//kiírás eleje
    if(count($talalatok) > 0){
        //'kiírjuk' mennyi, és mely számok
        $output .= ' találatok száma:'.count($talalatok);//ennyi
        $output .= ' | '.implode(',',$talalatok);//ezek
    }else{
        $output.=' nincs találat';
    }
    $output.='</h2>';
    echo $output;
}
echo '<pre>';
var_dump($sorsolas,$szelvenyek);