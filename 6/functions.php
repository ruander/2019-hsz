<?php
/**
 * Eljárás a számsorsolás szimulálására a lottójátékhoz
 * @param int $huzasok_szama
 * @param int $limit
 * @return array
 */
function lottoGeneralas($huzasok_szama = 5, $limit = 90){
    $szamok = [];

    if($huzasok_szama>$limit){////védelem:végtelen ciklus lenne,ha több szám kéne, mint amiből sorsolni lehet
        trigger_error('Rossz paraméterezés!');
    }else{

        while (count($szamok) < $huzasok_szama) {
            $szamok[] = rand(1, $limit);//új 'sorsolt' szám
            $szamok = array_unique($szamok);//ismétlődések megszüntetése
        }
        sort($szamok);//rendezés
    }
    return $szamok;//visszatérünk a generált tömbbel
}