<?php
//php kód
var_dump($_POST);
//ha nem üres a post tömb akkor van mit feldolgozni
if(!empty($_POST)){
    //'tisztázzuk' az adatokat és validáljunk
    $hiba = [];//üres hibatömb
    //név
    $nev = filter_input(INPUT_POST,'nev');
    //felesleges spacek eltávolítása a kapott string 2 végéről (van ltrim és rtrim is)
    $nev = trim($nev);
    //ne maradjon üres
    if($nev == ""){
        $hiba['nev'] = '<span class="error">Kötelező mező!</span>';
    }
    //email legyen email
    $email = filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL);//vagy email cim, vagy false
    if(!$email){
        $hiba['email'] = '<span class="error">Hibás formátum!</span>';
    }
    //jelszó legyen min 6 karakter
    $pass = filter_input(INPUT_POST,'password');
    if(strlen($pass) < 6 ){
        $hiba['password'] = '<span class="error">jelszó túl rövid!</span>';
    }
    //ha a hibatömb üres maradt, nincs hiba, tesszük adolgunk(feldolgozás)
    if(empty($hiba)){
        exit('nincs hiba');
    }

}

//isset használata
var_dump(isset($hiba['nev'])); //echo $var ->ez hiba mert nincs ilyen változó, csak az isset nem dob hibát nem létező változó
?><!doctype html>
<html>
<head>
    <title>A Zűrlapok működése</title>
    <meta charset="utf-8">
    <style>
        label{
            display: block;
        }
        .error {
            color:red;
            font-style: italic;
        }
    </style>
</head>
<body>
<h1>POST metódus</h1>
<form method="post">
    <label>Név
    <input type="text" name="nev" value="<?php echo filter_input(INPUT_POST,'nev'); ?>" placeholder="John Doe"> <?php
     if(isset($hiba['nev'])) echo $hiba['nev'];//ha létezik a hibatömb nev kulcsa (amit a file elején alakítunk ki ha van adat)
        ?></label>
    <label>Email
        <input type="email" name="email" value="<?php echo filter_input(INPUT_POST,'email'); ?>" placeholder="cim@email.hu">
        <?php
        if(isset($hiba['email'])) echo $hiba['email'];//ha létezik a hibatömb nev kulcsa (amit a file elején alakítunk ki ha van adat)
        ?>
    </label>
<!-- jelszó mező -->
    <label>Email
        <input type="password" name="password" value="" placeholder="******">
        <?php
        if(isset($hiba['password'])) echo $hiba['password'];
        ?>
    </label>
   <input type="submit" value="Elküld Post">
</form>
</body>
</html>