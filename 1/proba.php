<?php
$veletlenSzam = rand(10,20) ;//véletlen szám generálása 

//párosság ellenőrzése
if( $veletlenSzam % 2 == 0){
	//páros
	$color = 'red';
}else{
	//páratlan
	$color = 'black';
}
?><!doctype html>
<html lang="hu">
 <head>
  <title>PHP első óra</title>
  <meta charset="utf-8">
  <style>
  body {
	color:white;
	background: <?php echo $color; ?>;
  }
  </style>
 </head>
 <body>
 <?php 
 echo "<h1>a generált szám: $veletlenSzam</h1>";
 //php kód helye // operátor: egysoros komment
 /*
 több soros komment
 a kommentekből a kliens oldalon nem látszik semmi
 */
 echo 'Helo Világ!';// '' -> string operátor ; -> utasítás lezárása ; echo -> kiírja a mögötte található értéket a standard outputra
 
 $nev = "Horváth György";// $-> változó operátor; = -> értékadó operátor; "" -> string operátor
 echo '<br>';//sortörés kiírása, a html a php számára csupán egy string
 print $nev;
 
 //A változók
 
 //primitívek
 $szoveg = ", egy PHP tanonc.";// string
 $egesz = 1234;//integer vagy int
 $lebegoPontos = 132.23;// floating point number, float
 $logikai = true;//boolean, bool
 
 //műveletek változókkal
 echo '<pre>';
 var_dump($nev,$lebegoPontos,$szoveg,$egesz,$logikai);//információ bármilyen változó(k)ról
 echo '</pre>';
 
 $osszeg = $egesz + $lebegoPontos;
 echo "Az egesz és a lebegopontos összege: ".$osszeg;// . operátor -> konkatenátor
 
 $valami = $szoveg + $egesz;// nem azonos tipusok ezért warning!!!! mert a szöveget automatikusan konvertálnia kéne ami a 7es php ban már warningal jár
 echo '<br>'.$valami;//sortörés + változó tartalma
 
 $content = $nev . $szoveg;//nev és szoveg változó összefűzése
 
 echo "<br>$content";// "" között a primitív 'befordul'
 echo '<br>$content';// itt meg nem
 
 echo "<br>\$content";// \ -> escape operátor : az utána közvetlenül található karaktert kiemeli a nyelvi végrehajtásból
 echo '<br>'.$content;
 
 $datumIdo = date("Y-m-d H:i:s");//aktuális dátum formázva
 echo '<pre>'.var_export($datumIdo,true).'</pre>';//var export gyakorlati alkalmazása
 
 
 
 //php kód vége ?>
 <footer style="text-align:center">Az oldal betöltésekor a pontos idő: <?php echo $datumIdo; ?> </footer>
 </body>
</html>