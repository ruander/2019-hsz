<?php
//erőforrások
//var_dump('<pre>',$_SERVER);
$action = filter_input(INPUT_GET, 'act') ?: 'list';
$tid = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);//ez lesz akivel dolgozni kell (ID) vagy false
//var_dump($action);
$output = '';//ez lesz a kiírandó
//felhasználók 'regisztrálása'
//név,email,jelszó
//fileba gyüjtjük és visszaolvassuk, mindenki egy file-ba kerül
$dir = 'users/';//ez lesz a mappa
//ha nem létezne, létrehozzuk
if (!is_dir($dir)) mkdir($dir, 0644);
$userFile = 'list.json';//itt lesznek a userek
//ha létezik ilyen file, akkor beolvassuk és egy táblázatban megmutatjuk
$users=[];
if (file_exists($dir . $userFile)) {
    $users = json_decode(file_get_contents($dir . $userFile), true);
}
//var_dump($users);
//űrlap adatok feldolgozása/hibakezelés ha kell
if (!empty($_POST)) {
    $hiba = [];
    //name,email,tippek
    $name = trim(filter_input(INPUT_POST, 'name'));//mező értéken alap szűrése és a spacek eltávolítása
    //név minimum 3 karakter
    if (mb_strlen($name, "utf-8") < 3) {
        $hiba['name'] = '<span class="error">A név minimum 3 karakter kell legyen</span>';
    }
    //email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Nem érvényes formátum!</span>';
    }

    if (empty($hiba)) {
        //adatok tisztázása
        $newUser = [
          'name' => $name,
          'email' => $email,
          'password' => 'ésiudfg'
        ];
        //a users vagy üres, vagy tartalmazza a usereket, ehhez hozzáadjuk az ujat
        $users[]=$newUser;
        //jsonné alakítjuk és visszaírjuk a filet
        $contentToPut = json_encode($users);
        file_put_contents($dir.$userFile,$contentToPut);
        //visszairányítunk listázásra
        header('location:'.$_SERVER['PHP_SELF']);exit();
        //die('jók az adatok!');
    }
}
//switch alkalmazása a műveletek leválasztásához
switch ($action) {
    case 'delete':
        //echo 'törölni szeretnénk:' . $tid;
        //var_dump($users);
        if (is_int($tid)) {//mivel lehet nulla ezért nem elég az if($tid)
            unset($users[$tid]);//'felhasználó eltávolítása'

            sort($users);//kulcsok újrarendezése
            $users_json = json_encode($users);
            file_put_contents($dir . $userFile, $users_json);//új file kiírása (felülírása)
        }

        header('location:' . $_SERVER['PHP_SELF']);//$_SERVER -> rengeteg információt tartalmaz
        exit();
        break;

    case "update":
        echo 'szerkeszteni szeretnénk:' . $tid;
        echo '<a href="?">vissza a táblázathoz</a>';
        if (is_int($tid)) {//mivel lehet nulla ezért nem elég az if($tid)
            var_dump($users[$tid]);
        }
        break;

    case "create":
        $form = '<a href="?">vissza a táblázathoz</a>
            <form method="post">
            <fieldset>
                <legend>Felhasználó adatai</legend>
                <label>Email<sup>*</sup>
                    <input type="text" name="email" placeholder="email@cim.hu" value="' . filter_input(INPUT_POST, 'email') . '">';//űrlap elem értékének visszaírása
        if (isset($hiba['email'])) {//hiba 'befűzése' az űrlap elemhez ha van
            $form .= $hiba['email'];
        }
        $form .= '</label>
                <label>Név<sup>*</sup>
                    <input type="text" name="name" placeholder="John Doe" value="' . filter_input(INPUT_POST, 'name') . '">';//űrlap elem értékének visszaírása
        if (isset($hiba['name'])) {//hiba 'befűzése' az űrlap elemhez ha van
            $form .= $hiba['name'];
        }
        $form .= '</label>
            </fieldset>';
//form zárása és a gomb
        $form .= '<button type="submit">új felvitel</button>
</form>';
        $output .= $form;
        break;

    default:
        //ha nem üres amit kapunk, akkor tegyük egy táblázatba az elemeit
        $table = '<a href="?act=create">új felvitel</a>
            <table border="1"><tr>
                <th>ideiglenes azonosító</th>
                <th>név</th>
                <th>email</th>
                <th>művelet</th>
            </tr>';//fejléc
        foreach ($users as $itemId => $user) {
            $table .= '<tr>
                <td>' . $itemId . '</td>
                <td>' . $user['name'] . '</td>
                <td>' . $user['email'] . '</td>
                <td><a href="?act=update&amp;id=' . $itemId . '">módosít</a> | <a href="?act=delete&amp;id=' . $itemId . '">töröl</a></td>
            </tr>';//felhasználó sora
        }
        $table .= '</table>';
        $output .= $table;
        break;//kilépés a switchből
}

echo $output;
//styles
$styles = "
<style>
label {
    display:block;
}
.error {
    font-style: italic;
    color:red;
}
</style>";
echo $styles;