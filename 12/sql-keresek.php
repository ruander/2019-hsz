<?php
require_once "config/connect.php";//db kapcsolat $link

/*
$qry = "SELECT
	c.customernumber,customername
FROM orders o, customers c
WHERE c.customernumber=o.customerNumber
GROUP BY c.customernumber
ORDER BY COUNT(ordernumber) DESC";
*/
$qry = "SELECT 
	c.customernumber,
	customername,
	COUNT(ordernumber) db
FROM orders o
LEFT JOIN customers c 
ON c.customernumber = o.customernumber
GROUP BY c.customernumber 
ORDER BY db DESC
LIMIT 1";//kérés összeállítása
$result = mysqli_query($link,$qry) or die(mysqli_error($link));//lekérés
//jelen esetben a limit 1 miatt felesleges ciklusban kibontani
$row = mysqli_fetch_row($result);//kibontás
//echo '<pre>'.var_export($row,true).'</pre>';
echo '<h2>3. Melyik vevő rendelte eddig a legtöbbet (order darabszám) :</h2> '.implode(' | ',$row);
//quantityordered alapján még 1 táblát be kell csatolni
$qry = "SELECT 
	c.customernumber,
	customername,
	SUM(quantityordered) db
FROM orders o
LEFT JOIN customers c 
ON c.customernumber = o.customernumber
LEFT JOIN orderdetails od
ON od.ordernumber = o.ordernumber
GROUP BY c.customernumber 
ORDER BY  db DESC
LIMIT 1";
$result = mysqli_query($link,$qry) or die(mysqli_error($link));//lekérés
//jelen esetben a limit 1 miatt felesleges ciklusban kibontani
$row = mysqli_fetch_row($result);//kibontás
//echo '<pre>'.var_export($row,true).'</pre>';
echo '<h2>3. Melyik vevő rendelte eddig a legtöbbet (quantityordered) :</h2> '.implode(' | ',$row);

//érték alapján
$qry = "SELECT 
	c.customernumber,
	customername,
	SUM(quantityordered*priceeach) ertek
FROM orders o
LEFT JOIN customers c 
ON c.customernumber = o.customernumber
LEFT JOIN orderdetails od
ON od.ordernumber = o.ordernumber
GROUP BY c.customernumber 
ORDER BY ertek DESC
LIMIT 1";
$result = mysqli_query($link,$qry) or die(mysqli_error($link));//lekérés
//jelen esetben a limit 1 miatt felesleges ciklusban kibontani
$row = mysqli_fetch_row($result);//kibontás
//echo '<pre>'.var_export($row,true).'</pre>';
echo '<h2>3. Melyik vevő rendelte eddig a legtöbbet (érték USD) :</h2> '.implode(' | ',$row);

//4. Melyik japán vevő rendelte eddig a legnagyobb értéket?
$qry = "SELECT 
	c.customernumber,
	customername,
	SUM(quantityordered*priceeach) ertek
FROM orders o
LEFT JOIN customers c 
ON c.customernumber = o.customernumber
LEFT JOIN orderdetails od
ON od.ordernumber = o.ordernumber
WHERE country = 'japan'
GROUP BY c.customernumber 
ORDER BY ertek DESC
LIMIT 1";
$result = mysqli_query($link,$qry) or die(mysqli_error($link));//lekérés
//jelen esetben a limit 1 miatt felesleges ciklusban kibontani
$row = mysqli_fetch_row($result);//kibontás
//echo '<pre>'.var_export($row,true).'</pre>';
echo '<h2>3. Melyik vevő rendelte eddig a legtöbbet (érték USD) :</h2> '.implode(' | ',$row);

//5. Irodánként mennyit rendeltek?
$qry="SELECT off.city,
        SUM(quantityordered*priceeach) osszeg
        FROM offices off
        LEFT JOIN employees e 
        ON e.officecode = off.officecode
        LEFT JOIN customers c 
        ON salesrepemployeenumber = employeenumber
        LEFT JOIN orders o 
        ON o.customernumber = c.customernumber
        LEFT JOIN orderdetails od 
        ON od.ordernumber = o.ordernumber
        GROUP BY off.officecode";