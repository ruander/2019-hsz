<?php
//közös beállítások
$secret_key = '!S3cr3T_k3Y@';
$moduleDir = 'modules/';//modulok mappája
$moduleExtension =  '.php';//modulok kiterjesztése
//menu felépítése
$menuItems = [
  0 => [
      'title' => 'Vezérlőpult',
      'moduleName' => 'dashboard',
      'icon' => 'fa-tachometer-alt',
  ],
  1 => [
      'title' => 'Felhasználók',
      'moduleName' => 'users',
      'icon' => 'fa-user',
  ],
  2 => [
      'title' => 'Cikkek',
      'moduleName' => 'articles',
      'icon' => 'fa-book',
  ],
  3 => [
      'title' => 'Média',
      'moduleName' => 'mediaManager',
      'icon' => 'fa-images',
  ],
    4 => [
        'title' => 'Kuponjaim',
        'submenu' => false,
        'moduleName' => 'coupons',
        'icon' => 'fa-edit',
    ],
    5 => [
        'title' => 'sorsolás',
        'submenu' => false,
        'moduleName' => 'lots',
        'icon' => 'fa-edit',
        'privilege' => 'hgy@iworkshop.hu',//ő sorsolhat
    ],
];

$lang = 'hu';//nyelv
//szótár kiírásokhoz
$adminDictionary = [
  'hu' => [
      'status' => [
          0 => 'inaktív',
          1 => 'aktív'
      ],
      'actions' => [
          'create' => 'Új felvitel',
          'update'=> 'Módosítás',
          'delete' => 'Törlés',
          'list' => 'Lista'
      ],
  ]
];