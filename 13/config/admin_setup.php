<?php
include "connect.php";
//adatbázis telepítése
mysqli_query($link, 'SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO"') or die(mysqli_error($link));
mysqli_query($link, 'SET AUTOCOMMIT = 0') or die(mysqli_error($link));
mysqli_query($link, 'START TRANSACTION') or die(mysqli_error($link));
mysqli_query($link, 'SET time_zone = "+00:00"') or die(mysqli_error($link));

//táblák létrehozása
/*users*/
mysqli_query($link, 'CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(64) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(80) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT \'0-inaktív;1-aktív\',
  `time_created` datetime NOT NULL,
  `time_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8') or die(mysqli_error($link));

/*sessions*/
mysqli_query($link, 'CREATE TABLE IF NOT EXISTS `sessions` (
  `sid` varchar(64) NOT NULL,
  `spass` varchar(64) NOT NULL,
  `stime` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8') or die(mysqli_error($link));

/*articles*/
mysqli_query($link, 'CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `seo_title` varchar(255) NOT NULL,
  `lead` varchar(500) NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `author` varchar(64) NOT NULL,
  `time_published` datetime NOT NULL,
  `time_created` datetime NOT NULL,
  `time_updated` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8') or die(mysqli_error($link));

//alap adatok feltöltése
/*superuser - van már ilyen?*/
$row = mysqli_fetch_row(mysqli_query($link,'SELECT * FROM users WHERE id = 1 LIMIT 1' ));
if(empty($row)) {
    mysqli_query($link, "INSERT INTO `users` (`id`, `username`, `email`, `password`, `status`, `time_created`, `time_updated`) VALUES
(1, 'superadmin', 'hgy@iworkshop.hu', '$2y$10$.s/kBaSgYlccTEraKKNSpe3ZGkjXBSaUHx4gb1ay64BnGmnLp9UHi', 1, '2019-10-28 17:18:00', '2019-11-11 19:39:02');") or die(mysqli_error($link));
}
/*sample article - van már ilyen seo_title? mert unique*/
$row = mysqli_fetch_row(mysqli_query($link,"SELECT * FROM articles WHERE seo_title = 'teszt-cim'  LIMIT 1" ));
if(empty($row)) {
    mysqli_query($link, "INSERT INTO `articles` ( `title`, `seo_title`, `lead`, `content`, `status`, `author`, `time_published`, `time_created`, `time_updated`) VALUES
('Teszt cím', 'teszt-cim', 'teszt lead', 'teszt tartalom!', 1, 'John Doe', '2019-11-11 17:00:00', '2019-11-11 17:00:00', '2019-11-11 17:41:48')") or die(mysqli_error($link));
}
//
// Indexes for dumped tables
//

//
// Indexes for table `articles`
//
mysqli_query($link, "ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `seo_title` (`seo_title`);") or die(mysqli_error($link));

//
// Indexes for table `sessions`
//
mysqli_query($link, "ALTER TABLE `sessions`
  ADD PRIMARY KEY (`sid`);") or die(mysqli_error($link));

//
// Indexes for table `users`
//
mysqli_query($link, "ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);") or die(mysqli_error($link));

//
// AUTO_INCREMENT for dumped tables
//

//
// AUTO_INCREMENT for table `articles`
//
mysqli_query($link, "ALTER TABLE `articles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;") or die(mysqli_error($link));

//
// AUTO_INCREMENT for table `users`
//
mysqli_query($link, "ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;") or die(mysqli_error($link));
