<?php
//saját eljárások
/**
 * Menü html összeállítása
 * @param $menu | array
 * @return string
 */
function renderMenu($menu){
    $ret = '<nav class="admin-menu"><ul>';
    foreach($menu as $menuId => $menuItem){
        $ret .= '<li class=" '.$menuItem['icon'].'"><a href="?p='.$menuId.'">'.$menuItem['title'].'</a></li>';
    }
    $ret .= '</ul></nav>';
    return $ret;
}
/**
 * ADMINLTE Menü html összeállítása
 * @param $menu | array
 * @return string
 */
function renderMenuAdminLTE($menu){
    $ret = '<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">';
    foreach($menu as $menuId => $menuItem){
        $ret .= '<li class="nav-item">
                        <a href="?p='.$menuId.'" class="nav-link">
                            <i class="fas '.$menuItem['icon'].' nav-icon"></i>
                            <p>'.$menuItem['title'].'</p>
                        </a>
                    </li>';
    }
    $ret .= '</ul>';
    return $ret;
}

/**
 * Belépés ellenőrzése
 * @return bool
 */
function login(){
    global $secret_key;//az eljárás globálisnak látja a secret_keyt
    global $link;//db csatlakozás
    //email postból
    $email = filter_input(INPUT_POST,'email');

    $passDBResult = mysqli_query($link,"SELECT password,id,username FROM users WHERE email = '$email' LIMIT 1") or die(mysqli_error($link));
    $user = mysqli_fetch_row($passDBResult);
    $pwd = $user[0];//password a db-ből
    $pwd_for_check = filter_input(INPUT_POST,'pass');
    //secret_key
    $check = password_verify($pwd_for_check.$secret_key,$pwd);
    if($check){
        //felhasználó adatok
        $_SESSION["user"] = [
            'id' => $user[1],
            'username' => $user[2],
        ];
        //elmentjük a munkafolyamat adatokat
        $sid = session_id();
        $stime = time();//belépés pillanata mp ben
        $spass = sha1($sid.$user[1].$secret_key);//munkafolyamat jelszó -> mf azonosító+felh azonosító+secret_key elkódolva
        //eltároljuk a munkafolyamat adatait az adatbázisban
        mysqli_query($link,"INSERT INTO sessions(sid,spass,stime)
                                    VALUES('$sid','$spass',$stime)") or die(mysqli_error($link));
        //die(session_id());
    }
    return $check;
}
/**
 * Érvényes bejelentkezés ellenőrzése
 * @return bool
 */
function auth(){
    global $secret_key,$link;
    $sid = session_id();
    $spass = '';
    if(isset($_SESSION['user'])) {//nyelvi notice elkerülése
        $spass = sha1($sid . $_SESSION['user']['id'] . $secret_key);//mf jelszó ujra generálása
    }
    //mf azonosító alapján lekérjük a jelszót az adatbázisból ha van érvényes munkafolyamata
    $now = time();
    $expired = $now - 60 * 15; //15 perc
    mysqli_query($link, "DELETE FROM sessions WHERE stime < $expired") or die(mysqli_error($link));//töröljük a lejárt azonosításokat
    $qry = "SELECT spass FROM sessions WHERE sid = '$sid' LIMIT 1";//már csak érvényes mf-ok
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $spassDBRow = mysqli_fetch_row($result);
    if (!empty($spassDBRow) && $spassDBRow[0] == $spass) {
        //stime update
        mysqli_query($link, "UPDATE sessions SET stime = $now WHERE sid = '$sid' LIMIT 1") or die(mysqli_error($link));
        return true;
    } else {
        //mf adatok roncsolása ha van
        $_SESSION = [];
        session_destroy();
    }
    return false;
}

/**
 * kiléptetés
 */
function logout(){
    global $link;
    //db sor törlése
    mysqli_query($link,"DELETE FROM sessions WHERE sid =  '".session_id()."'") or die(mysqli_error($link));
    //mf roncsolása
    $_SESSION = [];
    session_destroy();
}
/**
 * eljárás a valuek visszaadására
 * @param $key elem neve a postban
 * @param string $dbData adatbázisból kapott érték
 * @return mixed|string //post adat, db adat vagy semmi
 */
function checkValue($key, $dbData  = ''){
    $postData = filter_input(INPUT_POST,$key);//ha létezik ilyen post elem
    if($postData !== null) return $postData;// akkor visszaadjuk azt

    return $dbData;//különben visszaadjuk a db adatot ami, ha nincs akkor üres string
}

/**
 * CheckBox default checker
 * @param $key - input name
 * @param string $dbData - data from database (0-1)
 * @return 'checked' | empty string
 */
function checkBox($key,$dbData = ''){
    if(!empty($_POST)){
        if(filter_input(INPUT_POST, $key)) return "checked";
        return '';
    }
    if($dbData) return "checked";
    return '';
}

/*
 * string kezelő eljárások
 */
function ekezettelenit($str) {
    $bad  = array('á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' ');
    $good = array('a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-');
    $str = mb_strtolower($str,"utf8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str,'-');//végződő - eltávolítása
    return $str;
}
///kupon feladat eljárásai
//hiba kiírásra
function hibaKiir($key, &$hiba){//hiba változó referenciaátadása
    //ha nem létezik a hiba tömb, (array_key__existnek az kell!) akkor létrehozunk üreset, ha létezik akkor nem változtatjuk
    $hiba = $hiba?:[];
    //ha létezik az adott kulcs akkor visszaadjuk a rajta található értéket
    if(array_key_exists($key,$hiba)){
        return $hiba[$key];
    }
    return;
}
function getInputValue($name, $row = [])
{//fallback: post,db érték, semmi

    $post_input = filter_input(INPUT_POST, $name);

    if ($post_input !== NULL) {
        return $post_input;
    }
    //ha nincs post, nézzük a db-t, ha a rownak van megfelelő kulcsa akkor jött adat a db-ből
    if (array_key_exists($name, $row)) {
        return $row[$name];
    }
    //egyébként meg semmi
    return;
}

//megjelenítéshez '-' hozzáadása a tokenhez
function hyphenate($str)
{
    return implode("-", str_split($str, 6));
}

