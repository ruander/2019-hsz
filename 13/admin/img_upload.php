<?php
$siteDir = '../site/';//itt van a publikus oldal
$uploadDir = 'images/';//ide szeretnénk feltölteni
$fileToDelete = filter_input(INPUT_GET,'fileToDelete');
//töröljük a filet ha kell
if($fileToDelete!='' && is_file($siteDir.$uploadDir.$fileToDelete)){
    //file törlése ha létezik
    unlink($siteDir . $uploadDir . $fileToDelete);
}
//mappa ellenőrzése
if(!is_dir($siteDir.$uploadDir)){
    mkdir($siteDir.$uploadDir,0755,true);
}
if (!empty($_POST)) {
    $hiba = [];
   //echo '<pre>'.var_export($_FILES,true).'';
   //file vizsgálata
    $file = $_FILES['fileToUpload']['tmp_name'];//ideiglenes hely a szerveren
    if( $_FILES['fileToUpload']['error']===0 //ez a kulcs ha nem nulla akkor feltöltési hiba van
        && is_uploaded_file($file)//feltöltés eredménye kell legyen
    ){
        //$fileName = $_FILES['fileToUpload']['name'];
        //ha a file kép akkor képként dolgozzuk fel, egyébként error
        $info = getimagesize($file);
        if(!$info){
            $hiba['fileToUpload']='Nem kép formátum!';//@todo : hiba kiírása a mező fölé
        }else{
            //max 7 elemű tömb
            if($info["mime"] ==  "image/jpeg"){//ha a kép jpeg
                //640*640 helyre kicsinyitsuk be méretarányosan
                $targetWidth = $targetHeight = 640;
                $origWidth = $info[0];//eredeti szélesség
                $origHeight = $info[1];//eredeti magasság
                $ratio = $origWidth/$origHeight;//képarány,  > 1:fekvő, < 1 álló

                if($ratio > 1 && $origWidth >= $targetWidth ){
                    //fekvő tájolás esetén a nagyobbik méret legyen a targetWidth
                    $newWidth=$targetWidth;
                    $newHeight = round($targetWidth/$ratio);//új magasság
                    //var_dump($newWidth,$newHeight);

                }elseif($origHeight >= $targetHeight){
                    //álló kép esetén
                    $newWidth=$targetHeight*$ratio;
                    $newHeight = $targetHeight;
                    //var_dump($newWidth,$newHeight);

                }
                //kiszámolt adatok alapján képművelet elvégzése
                $img = imagecreatefromjpeg($file);//forrás memóriába
                $canvas = imagecreatetruecolor($newWidth,$newHeight);//vászon
                imagecopyresampled ($canvas , $img ,  0 , 0 , 0, 0, $newWidth , $newHeight, $origWidth, $origHeight) ;
                /*
                 header("Content-Type: image/jpeg"); //böngésző lássa képnek ezt a filet
                */
                $fileName = $siteDir.$uploadDir.'valami.jpg';
                imagejpeg($canvas, $fileName, 100);


                //CROP
                $thumbWidth=$thumbHeight = 125;
                if($ratio > 1 ){
                    //fekvő kép esetén
                    $newHeight = $thumbHeight;//a kisebbik mérete!
                    $newWidth = round($thumbHeight * $ratio);//le fog lógni!
                    $canvas =  imagecreatetruecolor($thumbWidth,$thumbHeight);
                    //$img már van az előző műveletből
                    //x tengely offset
                    $y_offset = 0;
                    $x_offset = round(($newWidth - $thumbWidth)/2);//a 2 szélesség különbségének a fele
                    imagecopyresampled($canvas , $img , -$x_offset, -$y_offset, 0, 0,$newWidth,$newHeight,$origWidth,$origHeight);
                    header("Content-Type: image/jpeg");
                    imagefilter($canvas, IMG_FILTER_COLORIZE, 127, 0, 0);
                    imagejpeg($canvas, null, 100);
                    //memória felszabadítása
                    imagedestroy($img);
                    imagedestroy($canvas);
                }else{
                    //@todo: állóból crop
                }

            }


        }
    }
}
$form = '<form method="post" enctype="multipart/form-data">
<div class="form-group">
                    <label for="exampleInputFile">File feltöltése </label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input name="fileToUpload" type="file" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">File keresése</label>
                      </div>
                      <div class="input-group-append">
                        <button name="submit" class="input-group-text" id="">Feltöltés</button>
                      </div>
                    </div>
                  </div>
</form>';

echo $form;
//mappa listázása
if ($handle = opendir($siteDir.$uploadDir)) {
    $output = "'$uploadDir' tartalma:\n";
    /* mappa bejárása. */
    $output .='<ul>';
    while (false !== ($entry = readdir($handle))) {
        if($entry != '.' && $entry != '..') {//. és .. nem kell
            $output.='<li>' . $entry. '
            <a href="?fileToDelete='.$entry.'">X</a></li>';
        }// . és .. vizsgálat vége
    }//mappa bejáró ciklus vége
    $output.='</ul>';
    closedir($handle);
}
echo $output;