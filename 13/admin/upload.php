<?php
$siteDir = '../site/';//itt van a publikus oldal
$uploadDir = 'uploads/';//ide szeretnénk feltölteni
$fileToDelete = filter_input(INPUT_GET,'fileToDelete');
//töröljük a filet ha kell
if($fileToDelete!='' && is_file($siteDir.$uploadDir.$fileToDelete)){
    //file törlése ha létezik
    unlink($siteDir . $uploadDir . $fileToDelete);
}
//mappa ellenőrzése
if(!is_dir($siteDir.$uploadDir)){
    mkdir($siteDir.$uploadDir,0755,true);
}
if (!empty($_POST)) {
   echo '<pre>'.var_export($_FILES,true).'</pre>';
   //file vizsgálata
    $file = $_FILES['fileToUpload']['tmp_name'];//ideiglenes hely a szerveren
    if( $_FILES['fileToUpload']['error']===0 //ez a kulcs ha nem nulla akkor feltöltési hiba van
        && is_uploaded_file($file)//feltöltés eredménye kell legyen
    ){
        $fileName = $_FILES['fileToUpload']['name'];
        move_uploaded_file($file,$siteDir.$uploadDir.$fileName);//file átmozgatása a kívánt helyre
       //die($file);
    }
}
$form = '<form method="post" enctype="multipart/form-data">
<div class="form-group">
                    <label for="exampleInputFile">File feltöltése </label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input name="fileToUpload" type="file" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">File keresése</label>
                      </div>
                      <div class="input-group-append">
                        <button name="submit" class="input-group-text" id="">Feltöltés</button>
                      </div>
                    </div>
                  </div>
</form>';

echo $form;
//mappa listázása
if ($handle = opendir($siteDir.$uploadDir)) {
    $output = "'$uploadDir' tartalma:\n";
    /* mappa bejárása. */
    $output .='<ul>';
    while (false !== ($entry = readdir($handle))) {
        if($entry != '.' && $entry != '..') {//. és .. nem kell
            $output.='<li>' . $entry. '
            <a href="?fileToDelete='.$entry.'">X</a></li>';
        }// . és .. vizsgálat vége
    }//mappa bejáró ciklus vége
    $output.='</ul>';
    closedir($handle);
}
echo $output;