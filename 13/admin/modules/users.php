<?php
//users tábla CRUD
//erőforrások
$action = filter_input(INPUT_GET, 'act') ?: 'list';
$tid = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
//var_dump($_POST);
$output = '<!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fas fa-times"></i></button>
                    </div>
                </div>
                <div class="card-body">';//ez lesz a kiírandó
//felhasználók 'regisztrálása'
//név,email,jelszó
//fileba gyüjtjük és visszaolvassuk, mindenki egy file-ba kerül
$db_table = 'users';//ez lesz a db tábla amibe az adatok lesznek
//űrlap adatok feldolgozása/hibakezelés ha kell
if (!empty($_POST)) {
    $hiba = [];
    //name,email,tippek
    $username = trim(filter_input(INPUT_POST, 'username'));//mező értéken alap szűrése és a spacek eltávolítása
    //név minimum 3 karakter
    if (mb_strlen($username, "utf-8") < 3) {
        $hiba['username'] = '<span class="error">A név minimum 3 karakter kell legyen</span>';
    }
    //email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Nem érvényes formátum!</span>';
    } else {

        $qry = "SELECT id FROM $db_table WHERE email = '$email' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        //null ha nincs még ilyen, és tömb, ha már van, és ha tömb, akkor a 0 kulcsán van az ID akihez tartozik
        //echo '<pre>'.var_export($row,true).'</pre>';
        if (is_array($row) && $row[0] != $tid) {
            $hiba['email'] = '<span class="error">Már regisztrált email!</span>';
        }
    }
    //password
    $password = filter_input(INPUT_POST,'pass');
    //ha update van és az 1. jelszó mező nem üres akkor ellenőrzünk; illetve ha create van mindenképp
    if($action == "update" && $password != '' || $action == 'create') {
        //min 6 karakter
        if (mb_strlen($password, 'utf-8') < 6) {
            $hiba['pass'] = '<span class="error">min 6 karakter!</span>';
        } else {
            //pass1 rendben, nézzük repass ugyanaz e
            $repass = filter_input(INPUT_POST, 'repass');
            if ($password !== $repass) {
                $hiba['repass'] = '<span class="error">jelszavak nem egyeztek!</span>';
            } else {
                //jelszó oké, kódoljuk el (md5 elavult, de ...)
                $password = password_hash($password . $secret_key, PASSWORD_BCRYPT);
            }
        }
    }
    //status
    $status = filter_input(INPUT_POST,'status')?:0;
    if (empty($hiba)) {
        //adatok tisztázása
        $now = date('Y-m-d H:i:s');//datetime
        $user = [
            'username' => $username,
            'email' => $email,
            'password' => $password,
            'status' => $status,
            'time_created' => $now,
            'time_updated' => $now
        ];

        //update/crate szétválasztása
        if($action == 'create') {
            //uj felh. felvitele
           $qry = "INSERT INTO `$db_table` (`".implode("`,`",array_keys($user))."`) 
                    VALUES ('".implode("','",$user)."')";
        }else{//update esetben a jelszóra ügyelünk, hogy csak akkor modositsuk amikor tényleg szeretnénk
            unset($user['time_created']);//mert nem kell
            if($user['password'] == '') unset($user['password']);//ha nem kell vegyük ki
            $qry = "UPDATE $db_table SET ";
            foreach($user as $k => $v) {
                $qry .= "`$k` = '$v',";
            }
            $qry = rtrim($qry,',');//felesleges utolsó (,) eltávolítása
                    $qry .= " WHERE id = $tid LIMIT 1";
        }
        mysqli_query($link,$qry) or die(mysqli_error($link));//insert vagy error
        //visszairányítunk listázásra
        /**
         * @todo Meg kell oldani hogy a header warning eltünjön. Oka az hogy az index fileban van kiírás mielőtt a modul logikai működése megtörténne
         */
        header('location:'.$baseUrl);exit();
        //die('jók az adatok!');
    }
}
//switch alkalmazása a műveletek leválasztásához
switch ($action) {
    case 'delete':
        //echo 'törölni szeretnénk:' . $tid;
        //var_dump($users);
        if (is_int($tid)) {//mivel lehet nulla ezért nem elég az if($tid)
            mysqli_query($link,"DELETE FROM $db_table WHERE id = $tid LIMIT 1");
        }

        header('location:' . $baseUrl);//$_SERVER -> rengeteg információt tartalmaz
        exit();
        break;

    case "update":
        if (is_int($tid)) {//mivel lehet nulla ezért nem elég az if($tid)

            $qry = "SELECT id,username,email,`status` 
                    FROM $db_table 
                    WHERE id = $tid
                    LIMIT 1";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            $user = mysqli_fetch_assoc($result);
            //echo '<pre>' . var_export($user, true) . '</pre>';
        }
        //break;

    case "create":
        $form = '<a href="'.$baseUrl.'">vissza a táblázathoz</a>
            <form method="post">
            <fieldset>
                <legend>Felhasználó adatai</legend>
                <label>Email<sup>*</sup>
                    <input type="text" name="email" placeholder="email@cim.hu" value="' . checkValue('email',isset($user['email'])?$user['email']:''). '">';//űrlap elem értékének visszaírása
        if (isset($hiba['email'])) {//hiba 'befűzése' az űrlap elemhez ha van
            $form .= $hiba['email'];
        }
        $form .= '</label>
                <label>Név<sup>*</sup>
                    <input type="text" name="username" placeholder="John Doe" value="' . checkValue('username',isset($user['username'])?$user['username']:''). '">';//űrlap elem értékének visszaírása
        if (isset($hiba['username'])) {//hiba 'befűzése' az űrlap elemhez ha van
            $form .= $hiba['username'];
        }
        //jelszó1
        $form .= '</label>
                <label>Jelszó<sup>*</sup>
                    <input type="password" name="pass"  value="">';
        if (isset($hiba['pass'])) {//hiba 'befűzése' az űrlap elemhez ha van
            $form .= $hiba['pass'];
        }
        $form .= '</label>';
        //jelszó2
        $form .= '<label>Jelszó mégegyszer<sup>*</sup>
                    <input type="password" name="repass"  value="">';
        if (isset($hiba['repass'])) {//hiba 'befűzése' az űrlap elemhez ha van
            $form .= $hiba['repass'];
        }
        $form .= '</label>';
        //aktív - inaktív
        $form .= '<label><input type="checkbox" name="status" value="1" '.checkBox('status',isset($user['status'])?$user['status']:'').' > aktív?</label>';
        $form .= '</fieldset>';

//form zárása és a gomb
        $form .= '<button type="submit">'.$adminDictionary[$lang]['actions'][$action].'</button>
</form>';
        $output .= $form;
        break;

    default:
        //ha nem üres amit kapunk, akkor tegyük egy táblázatba az elemeit
        $table = '<a class="btn btn-sm btn-success" href="'.$baseUrl.'&amp;act=create">új felvitel</a>
            <table class="table table-striped"><tr>
                <th>ID</th>
                <th>név</th>
                <th>email</th>
                <th>státusz</th>
                <th>művelet</th>
            </tr>';//fejléc
        //felhasználók lekérése
        $qry = "SELECT id,username,email,`status` FROM $db_table";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        while ($user = mysqli_fetch_assoc($result)) {
            $table .= '<tr>
                <td>' . $user['id'] . '</td>
                <td>' . $user['username'] . '</td>
                <td>' . $user['email'] . '</td>
                <td>' . $adminDictionary[$lang]['status'][$user['status']] . '</td>
                <td><a href="'.$baseUrl.'&amp;act=update&amp;id=' . $user['id'] . '" class="btn btn-sm btn-outline-warning">módosít</a> <a href="'.$baseUrl.'&amp;act=delete&amp;id=' . $user['id'] . '" class="btn btn-sm btn-danger"><span class="fas fa-trash"></span></a></td>
            </tr>';//felhasználó sora
        }
        $table .= '</table>';
        $output .= $table;
        break;//kilépés a switchből
}
$output .= '</div>
                <!-- /.card-body -->
                <div class="card-footer">
                    Modul lábléc
                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->';

//styles
$styles = "
<style>
label {
    display:block;
}
.error {
    font-style: italic;
    color:red;
}
</style>";

/**
 * @todo: ez alapján a file alapján Articles modul elkészítése
 * --db table: articles (id,title,seo_title(255)(unique),lead(400),content(text),author(80),status(0-1),time_published,time_created,time_updated
 * --time_published, ha lehet datetime picker legyen
 * --settingsbe előkészítettem
 *
 */


