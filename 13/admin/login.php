<?php
require_once "../config/connect.php";
require_once "../config/settings.php";
require_once "../config/functions.php";
$msg = '';//kiírandó üzenet a form elé
session_start();//munkafolyamat indítása
if(!empty($_POST)){
        //password_verify
        if(!login()){
            $msg = '<div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-ban"></i> Hiba!</h5>
                  Nem megfelelő Email/jelszó páros!
                </div>';
        }
}
if(auth()){//ha be van lépve a mf, akkor irány az index.php
    header('location:index.php');exit();
}


$form = $msg.'<form method="post">
                <div class="input-group mb-3">
                    <input class="form-control" type="text" name="email" value="'.filter_input(INPUT_POST,'email').'" placeholder="email@cim.hu">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input class="form-control" type="password" name="pass" value="">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input type="checkbox" id="remember">
                            <label for="remember">
                                Emlékezz rám
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Belépés</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>';

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ruander PHP tanfolyam | Admin belépés</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="css/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="?"><b>PHP</b> Tanfolyam - Admin</a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Belépéshez írd be a bejelentkezési adatokat</p>

            <?php echo $form; ?>

            <div class="social-auth-links text-center mb-3">
                <p>- Vagy -</p>
                <a href="#" class="btn btn-block btn-primary">
                    <i class="fab fa-facebook mr-2"></i> Belépés Facebook-al
                </a>
                <a href="#" class="btn btn-block btn-danger">
                    <i class="fab fa-google-plus mr-2"></i> Belépés Google+ -al
                </a>
            </div>
            <!-- /.social-auth-links -->

            <p class="mb-1">
                <a href="?forgot-password.html">Elfelejtett jelszó</a>
            </p>
            <p class="mb-0">
                <a href="?register.html" class="text-center">Regisztráció</a>
            </p>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="js/adminlte.min.js"></script>

</body>
</html>

