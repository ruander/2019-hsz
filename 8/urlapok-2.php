<?php
//erőforrások
$limit = 90;
$huzasok_szama = 5;
$title = "<h1>$huzasok_szama/$limit Lottójáték</h1>";
//űrlap feldolgozása
echo $_POST['name'];
if (!empty($_POST)) {
    $hiba = [];
    //var_dump('<pre>',$_POST);
    //name,email,tippek
    $name = trim(filter_input(INPUT_POST,'name'));//mező értéken alap szűrése és a spacek eltávolítása
    //név minimum 3 karakter
    if(mb_strlen($name,"utf-8") < 3){
        $hiba['name']='<span class="error">A név minimum 3 karakter kell legyen</span>';
    }
    //email
    $email = filter_input(INPUT_POST,'email', FILTER_VALIDATE_EMAIL);
    if(!$email){
        $hiba['email']='<span class="error">Nem érvényes formátum!</span>';
    }
    //az eredeti tippsor szürés nélküli értékei a későbbi felhasználásra
    $eredetiTippek = filter_input_array(INPUT_POST,[
        'tippek' => [
            'flags'     => FILTER_REQUIRE_ARRAY,
        ],
    ])['tippek'];//egyből a tippek kulcson levő tippek tömböt adja vissza
    //tippsor ellenőrzése, filter input array bemutatása
    $args = [
            'tippek' => [
                        'filter'    => FILTER_VALIDATE_INT,
                        'flags'     => FILTER_REQUIRE_ARRAY,
                        'options'   => ['min_range' => 1, 'max_range' => $limit]
                        ],
            ];
    $myFields = filter_input_array(INPUT_POST,$args);//tippek szűrése egy lépésben a visszaadott tömb ha helyes értéket talál akkor az érték ha hibás akkor false értékkel tér vissza az adott kulcson
    /*
     ["tippek"]=> array(5)
    {
    [1]=> int(1)
    [2]=> bool(false)
    [3]=> int(5)
    [4]=> bool(false)
    [5]=> bool(false)
    }
     */
    foreach($myFields['tippek'] as $k => $v){

       if(!$v){
           $hiba['tippek'][$k] = '<span class="error">Nem érvényes formátum!</span>';
       }
    }
    //hibakezelés vége
    //var_dump($eredetiTippek);
    if(empty($hiba)){
        //hf tárold el fileba az adatokat egy mappába
        //és olvasd vissza az adatokat, ha sikeres

        //sikeres visszaolvasás és kiírás utábn die('ÉLJEN, kész vagyok');
    }
}
$form = '<form method="post">
            <fieldset>
                <legend>feladó adatai</legend>
                <label>Email<sup>*</sup>
                    <input type="text" name="email" placeholder="email@cim.hu" value="'.filter_input(INPUT_POST,'email').'">';//űrlap elem értékének visszaírása
        if(isset($hiba['email'])){//hiba 'befűzése' az űrlap elemhez ha van
            $form .= $hiba['email'];
        }
      $form .='</label>
                <label>Név<sup>*</sup>
                    <input type="text" name="name" placeholder="John Doe" value="'.filter_input(INPUT_POST,'name').'">';//űrlap elem értékének visszaírása
        if(isset($hiba['name'])){//hiba 'befűzése' az űrlap elemhez ha van
            $form .= $hiba['name'];
        }
      $form .='</label>
            </fieldset>';
//tippsor input mezők kialakítása
$tippsor = '<fieldset>
                <legend>tippek</legend>';//fejléc
//ciklus segítségével elkészítjük a mezőket
for ($i = 1; $i <= $huzasok_szama; $i++) {
    $tippsor .= '<label>Tipp '.$i.'<sup>*</sup>
                    <input type="text" name="tippek['.$i.']" placeholder="10" value="'.(isset($eredetiTippek[$i])?$eredetiTippek[$i]:'').'">';
    /*
     shorten if:
    if(condition){true}esle{false}
    condition ? true : false
     */
    //mezőhiba beillesztése a mező mögé
    if(isset($hiba['tippek'][$i])){//ha az adott kulcs létezik a hibatömbben, akkor az adott mezőn hiba volt
        $tippsor .= $hiba['tippek'][$i];
    }
    $tippsor .='</label>';
}
$tippsor .= '</fieldset>';
//tippsor formba illesztése
$form .= $tippsor;
//form zárása és a gomb
$form .= '<button type="submit">tippsor feladása</button>
</form>';
//stílusok változóba gyűjtése
$additional_styles = "<style>
    label {
        display:block;
    }
</style>";

//kiírandók
$output = $title;
$output .= $form;
$output .= $additional_styles;
echo $output;