<?php
$filename = 'testfile.txt';
echo '<pre>';//dumpok miatt
$handle = fopen($filename,'r');//filestream memóriába, ez resource tipusu
$content = fread($handle,8);//8byte beolvasása a fileból
var_dump($content);
$content = fread($handle,8);//újabb 8byte beolvasása a fileból
var_dump($content);

while(!feof($handle)){//a maradék ciklussal amig a filepointer a file végére nem ér
    $content = fread($handle,8);//újabb 8byte beolvasása a fileból
    var_dump($content);
}
fclose($handle);//memória felszabadítása
//file létrehozása
$filename = "testfile-iras.txt";
$handle = fopen($filename,'w');//megnyitás, ha nincs akkor készít egyet és resource-ként a memóriába helyezi //a kapcsoló, a file végére helyezett pointerrel nyitja meg
fwrite($handle,'teszt file tartalma...');//elhelyezzük a tartalmat a fileba
fclose($handle);

//~ugyanez functionnel
$contents = file_get_contents($filename);//teljes file tartalom stringbe (url is megy)
var_dump($contents);

$ujtartalom = "ez uj tartalom";
file_put_contents($filename,$ujtartalom,FILE_APPEND);//uj tartalom fileba írása
//most már ez a tartalma
var_dump(file_get_contents($filename));

//nem primitív értékek tárolása file-ban
$user = [
  'id' => 1,
  'name' => 'Horváth György',
  'email' => 'hgy@iworkshop.hu'
];
echo $user_as_string = serialize($user);//stringgé alakítás
//kiírás file-ba
$filename = 'user.txt';
file_put_contents($filename,$user_as_string);//tárolás file-ban
///////////////////
///             ///
///////////////////
$user_from_file = file_get_contents($filename);//beolvasás mint string
$user_test = unserialize($user_from_file);//visszalakítás tömbbé

//tesztelés
if($user === $user_test){/// === -> operátor, érték és tipusegyezés vizsgálat
    echo '<br>A teszt sikeres, a kiírt és a beolvasott adatok egyeznek!';
}
var_dump($user,$user_test);//tömb

//nem primitív adat tárolása 2 - csv
$filename = 'user.csv';
$handle = fopen($filename,'w');//a megnyitás módja választja ki hova kerül az adat és törlődik-e ami már volt, jelen állapotban, igen
//az első sor legyen a mezők neve (a tömb kulcsai)
fputcsv($handle,array_keys($user));//adat kiírása file-ba csv formátumban
fputcsv($handle,$user);//adat kiírása file-ba csv formátumban
fclose($handle);

////nem primitív adat tárolása 3 - json
$user_json = json_encode($user);
$filename = 'user.json';
file_put_contents($filename,$user_json);//fileba írás
/////////////////////////////////////////////
$user_json_from_file = file_get_contents($filename);//visszaolvasás
$user_array_from_json = json_decode($user_json,true);//a 2. paraméter miatt lesz tömb, egyébként alapértelmezetten objektumot ad
//teszt
if($user === $user_array_from_json){/// === -> operátor, érték és tipusegyezés vizsgálat
    echo '<br>A json teszt sikeres, a kiírt és a beolvasott adatok egyeznek!';
}
//file törlése
unlink($filename);

//mappakezelés
$dir = 'tesztmappa';
//mappa létezésének vizsgálata
if(is_dir($dir)){
    echo '<br>a mappa létezik!';
}else{
    echo '<br>a mappa nem létezik!!!!!';
}
//ha nem létezik hozzuk létre
if(!is_dir($dir)){
    mkdir($dir,0755, true);//a 3. paraméter a rekurziv mód azal a teljes mappautvonalat létrehozza ha nincs
}
//teszt hogy sikerült-e létrehozni
if(is_dir($dir)){
    echo '<br>a mappa létezik!';
}else{
    echo '<br>a mappa nem létezik!!!!!';
}
//üres mappa törlése
rmdir($dir);