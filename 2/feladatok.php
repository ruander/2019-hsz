<?php
//1. Készítsen egy olyan ciklust, amely egymás után menü feliratokat ír ki.
//fapad
for($i=0;$i<4;$i++){
    echo "<br>Menü";
}
//full extra a jelenlegi tudással
echo "<nav><ul>";//nav és lista nyitás
for($i=0;$i<4;$i++){
    echo "<li><a href='?menu=$i'>Menü $i</a></li>";//egy menüpont listaelemként, urlben átadva a kattintott menüpont
}
echo "</ul></nav>";//lista és nav zárás
//Készítsünk programot, amely kiszámolja az első 100 darab. természetes szám összegét, majd kiírja az eredményt. (Az összeg kiszámolásához vezessünk be egy változót, amelyet a program elején kinullázunk, a ciklusmagban pedig mindig hozzáadjuk a ciklusváltozó értékét, tehát sorban az 1, 2, 3, 4, ..., 100 számokat.)
echo "<h2>2.feladat</h2>";
$sum = 0;
for($i=1;$i<=100;$i++){
    $sum += $i;
}
echo 'Az összeg:'.$sum;
/*********házi feladatok.txt***********/
//1. Írjon egy php programot, amely kiszámolja és kiírja a 2 m élhosszúságú kocka felületét.
$a = 2;
$felulet = pow($a,2)*6;

//2.Írjon egy php programot, amely kiszámolja és kiírja a 2 m sugarú gömb térfogatát. A gömb térfogata V=4/3 r3pi.
$r=2;
$terfogat = 4/3* pow($r,3)*pi();

//3.Írjon egy php programot, amely kiszámolja és kiírja egy derékszögű háromszög átfogójának hosszát, ha a befogói 2 és 5m hosszúak.
$a = 2;
$b = 5;
//számoljuk ki a és b befogójú derékszögű háromszög átfogóját
$c =sqrt( pow($a,2)+pow($b,2) );
echo "Egy $a m és $b m befogójú derékszögű háromszög átfogója: $c m.";