<?php
//ciklus->ismétlődő programrész

//pl számok kiírása 1-50ig
$i=1;
echo "$i";//ez lesz a ciklusmagban, amit a ciklusban léptetünk
//a for ciklus
/* for(ciklusváltozó kezdeti értéke;belépési feltétel vizsgálata;ciklusváltozó léptetése){
    ciklusmag
}*/
for($i=1;$i<=50;$i++){
    echo "<br>$i";//ciklusmagban kiírjuk a ciklusváltozó pillanatnyi értékét
}
//$i most 51
//dobjunk 5 ször dobókockával és írjuk ki az eredményüket
for($i=1;$i<=5;$i++){
    echo "<br>A $i. dobás:".rand(1,6);//minden alkalommal új dobást generálunk és kiírjuk
}
//most írjuk ki a dobások összegét is 5-30
$sum = 0;//összeg kezdeti értéke
for($i=1;$i<=5;$i++){
    //eltároljuk az aktuális 'dobást' mert több műveletet is végzünk vele (kiírás,összegnövelés)
    $dobas = rand(1,6);
    $sum += $dobas;//$sum = $sum + $dobas;//összeg növelése az aktuális dobás értékével;operátor -> += :megnöveli a bal oldal értékét a jobb oldal értékkel
    echo "<br>A $i. dobás: $dobas";//kiírás
}
//összeg kiírásaa ciklus után
echo "<h3>A dobások összege: $sum</h3>";
//ugyanez a feladat továbbgondolva
//használjunk tömböt tároláshoz
$dobasok = [];
for($i=1;$i<=5;$i++){
    $dobasok[$i] = rand(1,6);
}
//a felépítet tömb a dobásokkal
var_dump($dobasok);
//a csodás implode
echo '<br>A dobások értékei: '.implode(',',$dobasok);
echo 'Az összegük:'.array_sum($dobasok);