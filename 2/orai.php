<?php
//pure php file -> csak php kód lesz, nem kell php zárótag ? >
$a = 3;
$b = 4;
//számoljuk ki a és b befogójú derékszögű háromszög átfogóját
$c = sqrt($a * $a + $b * $b);
echo "Egy $a m és $b m befogójú derékszögű háromszög átfogója: $c m.";
//ugyanaz eljárással
$c =sqrt( pow($a,2)+pow($b,2) );
echo "<br>Egy $a m és $b m befogójú derékszögű háromszög átfogója: $c m.";

//dobókocka szimulálása
$dobas = rand(1,6);//érték generálása
echo "<br>A dobás eredménye: $dobas";//érték kiírása

//nem primitív változók
$tomb = array();//oldschool üres tömb létrehozása
echo $tomb;//tömb nem kiírható szövegként
$tomb = [];//üres tömb -> $tomb újradeklarálása
echo "<pre>";
var_dump($tomb);//info a tömbről -> csak fejlesztés közben!
$tomb = [12,45,234.5,'alma', true];//tömb létrehozása automatikusan indexelt értékekkel
var_dump($tomb);
echo $tomb[3];//tömb egy elemének kiírása (ha az primitív, kiírható) -> 'alma'
$tomb[100] = 'Helo';//irányított indexre értékadás
var_dump($tomb);//a 4 és 100 közötti részt nem tölti fel
$tomb[] = "Horváth György";
var_dump($tomb);
$tomb['email'] = "hgy@ruander.hu";
var_dump($tomb);
echo "Az email cím: {$tomb['email']}";//tömb egy elemének kiírása idézőjelek között igényli a {} operátort a változó köré

//több dimenziós tömb
$user = [
  "id" => 5,
  "name" => "Horváth György",
  "email" => "hgy@ruander.hu",
  "kedvencek" => [ //beágyazott  asszociatív tömb
      'animal' => 'dog',
      'drink' => 'milk',
  ]
];//tömb felvétele csak irányított asszociatív indexekre
var_dump($user);
//érték kiírása alsóbb dimenzióból
echo "A user kedvenc állata: {$user['kedvencek']['animal']}";