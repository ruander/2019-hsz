<?php
//tömb felépítése asszociatív kulcsokkal hogy menüt tudjunk belőle létrehozni
$menu = [
  'home' => 'Kezdőlap',
  'about' => 'Rólunk',
  'services' => 'Szolgáltatások',
  'contact' => 'Kapcsolat',
];

//menü 'string' létrehozása:
$output = '';//it lesz a kiírandó string
$output .= '<nav><ul>';
//menüpontok közbeillesztése
/*
 foreach($tombneve as $kulcsneve => $ertekneve){
    ciklusmag (elérjük az aktuális  elem értékét $ertekneve változóban)
}
*/
//echo '<pre>';
foreach($menu as $key => $menuItem){
   // var_dump($key,$menuItem);
    $output .='<li><a href="?m='.$key.'">'.$menuItem.'</a></li>';
}
//html elemek lezárása
$output .='</ul></nav>';//rövidítve ez-> $output = $output . '</ul></nav>';
//kimenet kiírása egy lépésben
echo $output;