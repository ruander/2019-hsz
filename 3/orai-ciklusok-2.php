<?php
//a while ciklus
/*
while(belépési feltétel - condition){
    ciklusmag
}
 */
$i=1; //ezt fogjuk ciklusváltozónak használni
while($i<=5){
    echo "<br>$i";
    $i++;//ciklusváltozó léptetése
}
//5 dobás kockával - ua mint előző alkalom for ciklussal, tömbbe gyüjtjük a dobásokat
$tomb=[];//ebbe gyüjtjük a dobásokat
$i=0; //ezt fogjuk ciklusváltozónak használni
while($i<5){
    $tomb[]=rand(1,6);
    $i++;//ciklusváltozó léptetése
}
//kiírjuk a dobásokat
echo '<br>A dobások értékei: '.implode(',',$tomb);//dobások ,-vel elválasztva
echo '<br> a tömb elemszáma count($tomb): '.count($tomb);//tömb elemszáma /5
//használjuk fel a tömb elemszámát
$dobasok = [];
while( count($dobasok) < 5){
    $dobasok[]=rand(1,6);
}
/*
//ugyanez for ciklussal - nem tul szép hiányos paraméterezéssel
$dobasok = [];//újradeklarálás üresen
for(;count($dobasok)<5;){
    $dobasok[] = rand(1,6);
}
//kiírjuk a dobásokat
echo '<br>A dobások értékei: '.implode(',',$dobasok);//dobások ,-vel elválasztva
echo '<br> a dobások elemszáma count($dobasok): '.count($dobasok);//tömb elemszáma /5
*/
//Roulette szám szimulálása
/*
 * 'pörgessük ki a roulette számot és írjuk ki egyértelműen az eredményét -szín, párosság, kicsi-nagy
 */
$number = rand(0,36);
//szín
$red = [1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36];
if(in_array($number,$red)){
    $color = 'red';
}elseif($number == 0){
    $color = 'green';
}else{
    $color = 'black';
}
//páros?
$parossag = 'páros';//alapfeltételezés -todo: nulla?
if($number%2 == 1){
    $parossag = 'páratlan';
}

//kicsi/nagy
$nagysag = 'nagy';//alapfeltételezés -todo: nulla?
if($number<18){
    $nagysag = 'kicsi';
}
//nulla kivételek?
if($number == 0){
    $parossag = $nagysag =  'nulla';
}
echo "<h1 style='color:#fff;background: $color'>$number - $parossag - $nagysag</h1>";